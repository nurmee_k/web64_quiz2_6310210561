const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'schoolmusic',
    password : 'music',
    database : 'MusicSchoolSystem'
})

connection.connect();

const express = require('express');
const { hash } = require('bcrypt');
const req = require('express/lib/request');
const res = require('express/lib/response');
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken (req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/* CRUB Operation for MusicEvent Table */

app.get("/list_musicevent", (req, res) => {
    let query = "SELECT * from MusicEvent";
    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "static" : "400",
                        "message" : "Error querying from music db"               
                    })
        }else {
            res.json(rows)
        }
    });
})

app.post("/add_musicevent", (req, res) => {

    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `INSERT INTO MusicEvent 
                    (EventName, EventLocation) 
                    VALUES ('${event_name}','${event_location}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "static" : "400",
                        "message" : "Error inserting music into db"               
                    })
        }else {
            res.json({
                "static" : "200",
                "message" : "Adding event succesful"

            })
        }
    });
})
    
app.post("/update_musicevent", (req, res) => {

    let event_id = req.query.event_id
    let event_name = req.query.event_name
    let event_location = req.query.event_location

    let query = `UPDATE MusicEvent SET
                    EventName='${event_name}',
                    EventLocation='${event_location}'
                    WHERE EventID=${event_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "static" : "400",
                        "message" : "Error updating record"               
                    })
        }else {
            res.json({
                "static" : "200",
                "message" : "Updating event succesful"

            })
        }
    });
})

app.post("/delete_musicevent", (req, res) => {

    let event_id = req.query.event_id
   
    let query = `DELETE FROM MusicEvent WHERE EventID=${event_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "static" : "400",
                        "message" : "Error deleting record"               
                    })
        }else {
            res.json({
                "static" : "200",
                "message" : "Deleting record success"

            })
        }
    });
})

/* API for Processing Authorization */
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM MusicApplicant WHERE Username='${username}'`
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "static" : "400",
                        "message" : "Error querying from musicapplicant db"               
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].ApplicantID,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'} )
                    res.send(token)
                }else { res.send("Invalid username / password") }
            })
            
        }

    })
})

app.post("/register_applicant", (req, res) => {

    let musicapplicant_name = req.query.musicapplicant_name
    let musicapplicant_surname = req.query.musicapplicant_surname
    let musicapplicant_username = req.query.musicapplicant_username
    let musicapplicant_password = req.query.musicapplicant_password

    bcrypt.hash(musicapplicant_password, SALT_ROUNDS, (err, hash) => {
         let query = `INSERT INTO  MusicApplicant
                    (ApplicantName, ApplicantSurname, Username, Password, IsAdmin) 
                    VALUES ('${applicant_name}','${ applicant_surname}',
                            '${applicant_username}', '${hash}', false)`
        console.log(query)
        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "static" : "400",
                            "message" : "Error inserting data into db"               
                        })
            }else {
                res.json({
                    "static" : "200",
                    "message" : "Adding new user succesful"

                })
            }
        });

    })
});

app.listen(port, () => {
    console.log(`Now starting Music Shcool System Backend ${port}`)

}) 

connection.end();
